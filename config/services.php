<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '1754103211278075',
        'client_secret' => '0fa69f08ab14e26b8b8e28e24256ec46',
        'redirect' => 'https://ascad.pro/auth/facebook/callback',
    ],
    'google' => [
        'client_id' => '507563239997-ki6p2e912jhj2vru2j0gp3il5dg82bm1.apps.googleusercontent.com',
        'client_secret' => 'BLbmZDs9HohzCFOHkr0ltjjQ',
        'redirect' => 'https://ascad.pro/auth/google/callback',
    ],


];
