<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    function status()
    {
        return $this->belongsTo('App\Status');
    }

    function client()
    {
        return $this->belongsTo('App\Client');
    }

    function dealer()
    {
        return $this->belongsTo('App\Dealer');
    }

    function address()
    {
        return $this->belongsTo('App\Address');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }


}
