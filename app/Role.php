<?php

namespace App;

use Laratrust\Models\LaratrustRole;

/**
 * @property mixed name
 * @property mixed display_name
 * @property mixed description
 * @property mixed created_at
 * @property mixed updated_at
 */
class Role extends LaratrustRole
{
    //
}
