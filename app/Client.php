<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }


    public function dealer()
    {
        return $this->belongsTo('App\Dealer');
    }
    

    public function addresses()
    {
        return $this->hasMany('App\Address');
    }

    public function order(){
        return $this->hasMany('App\Order');
    }
}
