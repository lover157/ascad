<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Validator;
use function view;

/**
 * Created by PhpStorm.
 * User: edem
 * Date: 08.11.2017
 * Time: 23:06
 */
class ProfileController extends Controller
{


    public function create($hash)
    {
        $user = User::where('remember_token', '=', $hash)->firstOrFail();
        return view('profile.create', ['user' => $user]);
    }

    public function update(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'password' => 'required|min:6',
            'name' => 'required|max:255',
            'second_name' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('profile/'.$data['hash'])
                ->withErrors($validator)
                ->withInput();
        }

        $profile = User::findOrFail($data['id']);

        $profile->name = $data['name'];
        $profile->second_name = $data['second_name'];
        $profile->password = bcrypt($data['password']);
        $profile->activated = 1;
        $profile->remember_token = '';
        $profile->Save();

        return redirect('/users/show/' . $profile->id);
    }

}