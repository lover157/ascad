<?php

namespace App\Http\Controllers;


use App\Service;
use Illuminate\Http\Request;
use function redirect;
use function view;
use Session;


class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $services = Service::get()->toTree();
        return view('settings.services.all', ['services' => $services]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $services = Service::get()->toTree();
        return view('settings.services.create', ['services' => $services]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $attributes = [
            'name' => $request->name,
            'description' => $request->description,
        ];
        if ($request->service != NULL) {
            $parent = Service::find($request->service);
            $parent->children()->create($attributes);
        } else {
            $root = new Service($attributes);
            $root->save();
        }
        return redirect()->route('services.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $service = Service::findOrFail($id);

        return view('settings.services.update', ['service' => $service]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $service = Service::findOrFail($id);
        $service->name = $request->name;
        $service->description = $request->description;
        $service->save();

        Session::flash('notification', [
            'title' => 'Услуга изменена',
            'type' => 'success',
            'message' => ''
        ]);
        return redirect()->route('services.edit', ['id' => $service->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        $user = Service::findOrFail($request->id);
        $user->delete();

        Session::flash('notification', [
            'title' => 'Услуга удалена',
            'type' => 'success',
            'message' => ''
        ]);

        return redirect()->route('services.index');
    }


    public function apiGetById($id){
        $service = Service::findOrFail($id);
        return $service;
    }

    public function getList(){
        $worklist = Service::defaultOrder()->withDepth()->get()->toTree();
        $worklist = json_encode($worklist);
        $newWork = str_replace('"id"', '"value"', $worklist);
        $newWork = str_replace('"name"', '"label"', $newWork);
        $newWork = str_replace(',"children":[]', '', $newWork);

        return $newWork;
    }
}
