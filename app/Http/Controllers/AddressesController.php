<?php

namespace App\Http\Controllers;

use App\Address;
use Illuminate\Http\Request;
use function redirect;

class AddressesController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request)
    {
        $address = new Address();
        $address->client_id = $request->client_id;
        $address->locality = $request->locality;
        $address->street = $request->street;
        $address->object_number = $request->object_number;
        $address->user_id = $request->user_id;

        $address->save();

        return redirect()->route('clients.show', ['id' => $request->client_id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request)
    {
        $address = Address::findOrFail($request->id);
        $address->locality = $request->locality_upd;
        $address->street = $request->street_upd;
        $address->object_number = $request->object_number_upd;

        $address->save();
        return redirect()->route('clients.show', ['id' => $request->client_id]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function destroy(Request $request)
    {
        $address = Address::findOrFail($request->id);
        $address->delete();
        return redirect()->route('clients.show', ['id' => $request->client_id]);
    }

    public function byClient($id){
        $addresses = Address::where('client_id', $id)->get();
        return $addresses;
    }
}
