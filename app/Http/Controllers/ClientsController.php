<?php

namespace App\Http\Controllers;

use App\Dealer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Client;

use Validator;
use Session;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::with(['user', 'dealer'])->get();
        return view('clients.index', ['clients' => $clients]);
    }

    public function list(){
        $clients = Client::all();
        return $clients;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dealers = Dealer::all();
        return view('clients.create', ['dealers' => $dealers]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'second_name' => 'required|max:255',
            'third_name' => 'max:255',
            'email' => 'required|email|max:255'
        ]);

        if ($validator->fails()) {
            Session::flash('notification', [
                'title' => 'Ошибка создания',
                'type' => 'error',
                'message' => 'Проверьте правильность введеных данных'
            ]);
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $client = new Client();
        $client->name = $request->name;
        $client->second_name = $request->second_name;
        $client->third_name = $request->third_name;
        $client->email = $request->email;
        $client->phone1 = preg_replace('/[^0-9]/', '', $request->phone1);
        $client->phone2 = preg_replace('/[^0-9]/', '', $request->phone2);
        $client->comment = $request->comment;
        $client->dealer_id = $request->dealer_id;
        $client->user_id = $request->user_id;
        $client->save();
        return redirect()->route('clients.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = Client::findOrFail($id);
        return view('clients.show', ['client' => $client]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::findOrFail($id);
        $dealers = Dealer::all();

        return view('clients.edit', ['client' => $client, 'dealers' => $dealers]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'second_name' => 'required|max:255',
            'third_name' => 'max:255',
            'email' => 'required|email|max:255',
        ]);

        if ($validator->fails()) {
            Session::flash('notification', [
                'title' => 'Ошибка создания',
                'type' => 'error',
                'message' => 'Проверьте правильность введеных данных'
            ]);
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $client = Client::findOrFail($id);
        $client->name = $request->name;
        $client->second_name = $request->second_name;
        $client->third_name = $request->third_name;
        $client->email = $request->email;
        $client->phone1 = preg_replace('/[^0-9]/', '', $request->phone1);
        $client->phone2 = preg_replace('/[^0-9]/', '', $request->phone2);
        $client->comment = $request->comment;
        $client->dealer_id = $request->dealer_id;
        $client->user_id = $request->user_id;
        $client->save();

        Session::flash('notification', [
            'title' => 'Клиент изменен',
            'type' => 'success',
            'message' => $client->name . ' ' . $client->second_name,
        ]);

        return redirect()->route('clients.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        $client = Client::findOrFail($request->id);
        $client->delete();
        Session::flash('notification', [
            'title' => 'Клиент удален',
            'type' => 'success',
            'message' => $client->name . ' ' . $client->second_name,
        ]);

        return redirect()->route('clients.index');
    }

    public function findByPhone($phone)
    {
        $client = Client::where('phone1', $phone)->orWhere('phone2', $phone)->first();
        return $client;
    }

}
