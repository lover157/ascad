<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dealer;
use function redirect;

use Validator;
use Session;

class DealersController extends Controller
{

    public function index()
    {
        $dealers = Dealer::all();
        return view('dealers.index', ['dealers' => $dealers]);
    }

    public function create()
    {
        return view('dealers.create');
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'second_name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'phone' => 'required'
        ]);

        if ($validator->fails()) {
            Session::flash('notification', [
                'title' => 'Ошибка создания',
                'type' => 'error',
                'message' => 'Проверьте правильность введеных данных'
            ]);

            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $dealer = new Dealer;
        $dealer->name = $request->name;
        $dealer->second_name = $request->second_name;
        $dealer->second_name = $request->second_name;
        $dealer->phone = preg_replace('/[^0-9]/', '', $request->phone);

        $dealer->email = $request->email;
        $dealer->save();
        Session::flash('notification', [
            'title' => 'Дилер успешно создан',
            'type' => 'success',
        ]);
        return redirect()->route('dealers.index');

    }


    public function edit($id)
    {
        $dealer = Dealer::findOrFail($id);
        return view('dealers.edit', ['dealer' => $dealer]);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'second_name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'phone' => 'required'
        ]);

        if ($validator->fails()) {
            Session::flash('notification', [
                'title' => 'Ошибка создания',
                'type' => 'error',
                'message' => 'Проверьте правильность введеных данных'
            ]);

            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $dealer = Dealer::findOrFail($id);
        $dealer->name = $request->name;
        $dealer->second_name = $request->second_name;
        $dealer->second_name = $request->second_name;
        $dealer->phone = preg_replace('/[^0-9]/', '', $request->phone);

        $dealer->email = $request->email;
        $dealer->save();

        Session::flash('notification', [
            'title' => 'Диллер успешно изменен',
            'type' => 'success',
            'message' => ''
        ]);

        return redirect()->route('dealers.index');
    }

    public function destroy(Request $request)
    {
        $dealer = Dealer::findOrFail($request->id);
        $dealer->delete();
        Session::flash('notification', [
            'title' => 'Дилер успешно удален',
            'type' => 'success',
            'message' => ''
        ]);
        return redirect()->route('dealers.index');
    }


}
