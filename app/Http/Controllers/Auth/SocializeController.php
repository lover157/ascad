<?php


namespace App\Http\Controllers\Auth;

use App\SocialProvider;
use App\User;

use App\Http\Controllers\Controller;
use function redirect;
use Socialite;


class SocializeController extends Controller
{

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }


    public function handleProviderCallback($provider)
    {
        try {
            $socialUser = Socialite::driver($provider)->user();
        } catch (\Exception $e) {
            return redirect('/login');
        }
        //check if we have logged provider
        $socialProvider = SocialProvider::where('provider_id', $socialUser->getId())->first();
        $user = User::where('email', $socialUser->getEmail())->first();

        if (!$user) return redirect('/login');

        if (!$socialProvider) {
            $user->socialProviders()->create(
                ['provider_id' => $socialUser->getId(), 'provider' => $provider]
            );
        }

        auth()->login($user);


        return redirect()->route('home');
    }
}