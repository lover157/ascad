<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Role;
use App\Permission;

use App\Http\Requests\StoreUserRequest;
use Illuminate\Support\Facades\Password;

use App\Mail\CreateUser;

use Intervention\Image\Facades\Image;

use Carbon\Carbon;

use Session;
use Mail;
use Validator;


class UsersController extends Controller
{

    public function index(Request $request)
    {
        $users = User::all();
        return view('users.all', ['users' => $users]);
    }

    public function create()
    {
        $roles = Role::all();
        return view('users.create', ['roles' => $roles]);
    }

    public function store(StoreUserRequest $request)
    {
        $user = new User();
        $user->email = $request->email;
        $user->password = bcrypt($request->email . time());
        $user->remember_token = Password::createToken($user);
        $user->save();

        if ($request->roles) {
            $user->syncRoles($request->roles);
        }

        Mail::to($user->email)->send(new CreateUser($user->remember_token));

        Session::flash('notification', [
            'title' => 'Пользователь создан',
            'type' => 'success',
            'message' => $user->email
        ]);
        return redirect()->route('users.index');
    }

    public function show($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::all();
        $rolesAssigned = $user->roles->pluck('id');
        return view('users.one', ['user' => $user, 'roles' => $roles, 'rolesAssigned' => $rolesAssigned]);
    }


    public function update(Request $request, $id)
    {
        $data = $request->all();
        $user = User::findOrFail($id);


        if ($request->file('avatar')) {
            $avatar = $request->file('avatar');
            $avatarName = $id . time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(250, 250)->save(public_path('storage/avatars/' . $avatarName));
            $user->avatar = $avatarName;
        }

        if ($request->password) {
            $user->password = bcrypt($data['password']);
        }
        if ($request->roles) {
            $user->syncRoles($request->roles);
        }
        $user->name = $data['name'];
        $user->second_name = $data['second_name'];
        $user->phone = preg_replace('/[^0-9]/', '', $data['phone']);
        if ($data['birth_date']) {
            $user->birth_date = Carbon::createFromFormat('d/m/Y', $data['birth_date'])->toDateString();
        }
        $user->Save();

        Session::flash('notification', [
            'title' => 'Пользователь изменен',
            'type' => 'success',
            'message' => $user->email
        ]);
        return redirect()->route('users.show', ['id' => $id]);
    }

    public function destroy(Request $request)
    {
        $user = User::findOrFail($request->id);
        $user->syncRoles([]);
        $user->syncPermissions([]);
        $user->delete();

        Session::flash('notification', [
            'title' => 'Пользователь удален',
            'type' => 'success',
            'message' => $user->email
        ]);

        return redirect()->route('users.index');
    }

    public function ban($id)
    {
        $user = User::findOrFail($id);
        $user->banned = 1;
        $user->save();
        Session::flash('notification', [
            'title' => 'Пользователь заблокирован',
            'type' => 'success',
            'message' => $user->email
        ]);
        return back();
    }

    public function unBan($id)
    {
        $user = User::findOrFail($id);
        $user->banned = 0;
        $user->save();
        Session::flash('notification', [
            'title' => 'Пользователь разблокирован',
            'type' => 'success',
            'message' => $user->email
        ]);
        return back();
    }
}
