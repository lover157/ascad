<?php

namespace App\Http\Controllers;

use App\Role;
use App\Permission;
use App\User;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Session;

use function redirect;
use function view;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $roles = Role::all();
        return view('roles.all')->withRoles($roles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $permissions = Permission::all();
        return view('roles.create')->withPermissions($permissions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        //TODO VALIDATE + ERRORS
        $newRole = new Role;
        $newRole->name = $request->name;
        $newRole->display_name = $request->display_name;
        $newRole->description = $request->description;
        $newRole->created_at = Carbon::now();
        $newRole->save();

        Session::flash('notification', [
            'title' => 'Создана новая роль',
            'type' => 'success',
            'message' => $newRole->name
        ]);

        return redirect()->route('roles.show', ['id' => $newRole['id']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $role = Role::where('id', $id)->with('permissions')->first();
        $assignedPermissions = $role->permissions->pluck('id')->toArray();
        $permissions = Permission::all();
        $users = User::whereRoleIs($role['name'])->get();
        //ToDo Assigned Users
        //ToDo Assigned Permissions

        return view('roles.one', ['role' => $role, 'allPermissions' => $permissions, 'users' => $users, 'assignedPermissions' => $assignedPermissions]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $role = Role::where('id', $id)->with('permissions')->first();
        $assignedPermissions = $role->permissions->pluck('id')->toArray();
        $permissions = Permission::all();
        $users = User::whereRoleIs($role['name'])->get();

        //ToDo Assigned Users
        //ToDo Assigned Permissions

        return view('roles.edit', ['role' => $role, 'allPermissions' => $permissions, 'users' => $users, 'assignedPermissions' => $assignedPermissions]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
//        dd($request->all());

        //TODO VALIDATION

        $role = Role::findOrFail($id);
        $role->name = $request->name;
        $role->display_name = $request->display_name;
        $role->description = $request->description;
        $role->save();

        $role->syncPermissions($request->permissions);

        Session::flash('notification', [
            'title' => 'Изменена роль',
            'type' => 'success',
            'message' => $role->display_name
        ]);

        return redirect()->route('roles.edit', ['id' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        Role::destroy($id);
        return redirect()->route('roles.index');
    }
}
