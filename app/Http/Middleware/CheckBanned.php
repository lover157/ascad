<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class CheckBanned
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->banned == 1) {
            return redirect()->route('user-banned');
        }

        return $next($request);
    }
}
