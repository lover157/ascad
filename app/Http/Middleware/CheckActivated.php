<?php

namespace App\Http\Middleware;

use Auth;

use Closure;

class CheckActivated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->activated == 0) {
            return redirect()->route('not-activated');
        }

        return $next($request);
    }
}
