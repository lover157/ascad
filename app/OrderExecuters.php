<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderExecuters extends Model
{
    function order()
    {
        return $this->belongsTo('App\Order');
    }

    function user()
    {
        return $this->belongsTo('App\User');
    }
}
