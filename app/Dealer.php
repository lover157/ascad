<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dealer extends Model
{
    protected $fillable = [
        'name', 'second_name'
    ];

    public function clients()
    {
        return $this->hasMany('App\Client');
    }
}
