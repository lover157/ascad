<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Service extends Model
{
    use NodeTrait;

    protected $fillable = ['name', 'description'];

    protected $hidden = [
        'created_at', 'updated_at', 'description', 'depth', 'parent_id', '_lft', '_rgt'
    ];
}
