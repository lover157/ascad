<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use function route;

class CreateUser extends Mailable
{
    use Queueable, SerializesModels;

    protected $hash;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($hash)
    {
        $this->hash = $hash;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $link = route('profile.create', ['hash' => $this->hash]);
        return $this->view('mails.create_user')->with(['link' => $link]);
    }
}
