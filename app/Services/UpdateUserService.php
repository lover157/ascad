<?php

use App\Http\Requests\UpdateUserRequest;
use App\User;
use Illuminate\Support\Facades\Password;

class UpdateUserService
{
    public function update($id, UpdateUserRequest $request)
    {
        $user = User::findOrFail($id);
        $user->email = $request->email;
        $user->password = bcrypt($request->email . time());
        $user->remember_token = Password::createToken($user);
        $user->save();

        return $user;
    }
}