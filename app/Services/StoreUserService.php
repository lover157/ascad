<?php

namespace App\Services;

use App\User;
use Illuminate\Support\Facades\Password;

class StoreUserService
{

    public function make($request)
    {
        $user = new User();
        $user->email = $request->email;
        $user->password = bcrypt($request->email . time());
        $user->remember_token = Password::createToken($user);
        $user->save();

        return $user;
    }
}