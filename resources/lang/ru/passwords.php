<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль должен быть не меннее 6 символов',
    'reset' => 'Ваш пароль изменен!',
    'sent' => 'Мы отправили Вам ссылку на восстановление пароля',
    'token' => 'Этот код воостановления пароля не правильный.',
    'user' => "Мы не можен найти пользователя с этим email-ом",

];
