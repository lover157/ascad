<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Такой пользователь не найден',
    'throttle' => 'Доступ заблокирован, проверьте ваши данные и повторите через :seconds секунд.',
    'attributes' => [
        'email' => 'Электронный адрес',
        'password' => 'Пароль',
    ],
    'custom' => [
        'email' => [
            'required' => 'Нам необходимо знать Ваш электронный адрес!',
        ],
        'password' =>[
            'required' => 'Поле пароль обязательно для заполнения'
        ]
    ],
];
