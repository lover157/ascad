@extends('layouts.app')
@section('pageTitle', 'Ничего не найдено')


@section('content')
    <div class="error-page">
        <h2 class="headline text-yellow"> 404</h2>

        <div class="error-content">
            <h3><i class="fa fa-warning text-yellow"></i> Упс! Страница не найдена.</h3>

            <p>
                Мы не смогли найти нужную вам страницу. <br/>
                Может Вы хотите перейти <a href="{{ url()->previous() }}">назад</a>, или попробуйте поспользоваться поисковой формой
            </p>

            <form class="search-form">
                <div class="input-group">
                    <input type="text" name="search" class="form-control" placeholder="Search">

                    <div class="input-group-btn">
                        <button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
                <!-- /.input-group -->
            </form>
        </div>
        <!-- /.error-content -->
    </div>

@endsection