@extends('layouts.auth')

@section('content')
    <div class="login-box">
        <div class="login-logo">
            <a href="#"><b>AS</b>CAD</a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Ваша учетная запись заблокирована! <br/>
                Если это ошибка, свяжитесь с директором предприятия
            </p>
        </div>
        <!-- /.login-box-body -->
    </div>
@endsection
