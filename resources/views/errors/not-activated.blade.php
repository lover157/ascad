@extends('layouts.auth')

@section('content')
    <div class="login-box">
        <div class="login-logo">
            <a href="#"><b>AS</b>CAD</a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Ваша учетная запись не активирована <br/>
              Писмо с активацией было отправлено к вам на почту, если письмо не пришло, свяжитесь с директором предприятия
            </p>
        </div>
        <!-- /.login-box-body -->
    </div>
@endsection
