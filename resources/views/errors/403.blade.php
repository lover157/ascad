@extends('layouts.app')
@section('pageTitle', 'Ничего не найдено')


@section('content')
    <div class="error-page">
        <h2 class="headline text-red"> 403</h2>

        <div class="error-content">
            <h3><i class="fa fa-warning text-yellow"></i> У вас нет доступа к этой странице!</h3>
        </div>
        <!-- /.error-content -->
    </div>

@endsection