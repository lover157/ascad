@extends('layouts.app')
@section('pageTitle', 'Пользователи')

@section('content')
    <div class="row">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle" src="/storage/avatars/{{$user['avatar']}}"
                         alt="{{$user['name']}} {{$user['second_name']}}">

                    <h3 class="profile-username text-center">{{$user['name']}} {{$user['second_name']}}</h3>
                    <?php $userRoles = $user->roles?>

                    @foreach($userRoles as $role)
                        <p class="text-muted text-center">{{$role->display_name}} </p>
                    @endforeach
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>Задачи в работе</b> <a class="pull-right">1,322</a>
                        </li>
                        <li class="list-group-item">
                            <b>Завершено задач</b> <a class="pull-right">543</a>
                        </li>
                        <li class="list-group-item">
                            <b>Всего задач</b> <a class="pull-right">13,287</a>
                        </li>
                    </ul>

                    <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>

                    @if(Auth::user()->id != $user->id)
                        @if($user['banned'])
                            <a href="#" class="btn btn-warning btn-block" data-toggle="modal"
                               data-target="#unbanUser">Разблокировать<b></b></a>
                        @else
                            <a href="#" class="btn btn-warning btn-block" data-toggle="modal"
                               data-target="#banUser">Заблокировать<b></b></a>
                        @endif
                    @endif



                    <a href="#" class="btn btn-danger btn-block" data-toggle="modal" data-target="#deleteUser"><b>Удалить</b></a>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <!-- About Me Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Обо мне</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if( $user['phone'])
                        <strong><i class="fa fa-phone margin-r-5"></i> Номер телефона</strong>
                        <p class="text-muted">
                            <a href="tel:{{$user['phone']}}">
                                +{{$user['phone']}}
                            </a>
                        </p>
                        <hr>
                    @endif


                    @if( $user['email'])
                        <strong><i class="fa fa-envelope margin-r-5"></i> Email</strong>
                        <p class="text-muted">
                            <a href="mailto:{{$user['email']}}">
                                {{$user['email']}}
                            </a>
                        </p>
                        <hr>
                    @endif

                    @if( $user['birth_date'])
                        <strong><i class="fa fa-birthday-cake margin-r-5"></i> День рождения</strong>

                        <p class="text-muted">
                            {{$user['birth_date']}}
                        </p>
                        <hr>
                    @endif
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#timeline" data-toggle="tab">Timeline</a></li>
                    @if(Auth::user()->id === $user['id'] || Auth::user()->can('update-user'))
                        <li><a href="#settings" data-toggle="tab">Настройки профиля</a></li>
                    @endif
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="timeline">
                        <!-- The timeline -->
                        <ul class="timeline timeline-inverse">
                            <!-- timeline time label -->
                            <li class="time-label">
                        <span class="bg-red">
                          10 Feb. 2014
                        </span>
                            </li>
                            <!-- /.timeline-label -->
                            <!-- timeline item -->
                            <li>
                                <i class="fa fa-envelope bg-blue"></i>

                                <div class="timeline-item">
                                    <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>

                                    <h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>

                                    <div class="timeline-body">
                                        Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                                        weebly ning heekya handango imeem plugg dopplr jibjab, movity
                                        jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                                        quora plaxo ideeli hulu weebly balihoo...
                                    </div>
                                    <div class="timeline-footer">
                                        <a class="btn btn-primary btn-xs">Read more</a>
                                        <a class="btn btn-danger btn-xs">Delete</a>
                                    </div>
                                </div>
                            </li>
                            <!-- END timeline item -->
                            <!-- timeline item -->
                            <li>
                                <i class="fa fa-user bg-aqua"></i>

                                <div class="timeline-item">
                                    <span class="time"><i class="fa fa-clock-o"></i> 5 mins ago</span>

                                    <h3 class="timeline-header no-border"><a href="#">Sarah Young</a> accepted your
                                        friend request
                                    </h3>
                                </div>
                            </li>
                            <!-- END timeline item -->
                            <!-- timeline item -->
                            <li>
                                <i class="fa fa-comments bg-yellow"></i>

                                <div class="timeline-item">
                                    <span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>

                                    <h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>

                                    <div class="timeline-body">
                                        Take me to your leader!
                                        Switzerland is small and neutral!
                                        We are more like Germany, ambitious and misunderstood!
                                    </div>
                                    <div class="timeline-footer">
                                        <a class="btn btn-warning btn-flat btn-xs">View comment</a>
                                    </div>
                                </div>
                            </li>
                            <!-- END timeline item -->
                            <!-- timeline time label -->
                            <li class="time-label">
                        <span class="bg-green">
                          3 Jan. 2014
                        </span>
                            </li>
                            <!-- /.timeline-label -->
                            <!-- timeline item -->
                            <li>
                                <i class="fa fa-camera bg-purple"></i>

                                <div class="timeline-item">
                                    <span class="time"><i class="fa fa-clock-o"></i> 2 days ago</span>

                                    <h3 class="timeline-header"><a href="#">Mina Lee</a> uploaded new photos</h3>

                                    <div class="timeline-body">
                                        {{--<img src="http://placehold.it/150x100" alt="..." class="margin">--}}
                                        {{--<img src="http://placehold.it/150x100" alt="..." class="margin">--}}
                                        {{--<img src="http://placehold.it/150x100" alt="..." class="margin">--}}
                                        {{--<img src="http://placehold.it/150x100" alt="..." class="margin">--}}
                                    </div>
                                </div>
                            </li>
                            <!-- END timeline item -->
                            <li>
                                <i class="fa fa-clock-o bg-gray"></i>
                            </li>
                        </ul>
                    </div>
                    <!-- /.tab-pane -->
                    @if(Auth::user()->id === $user['id'] || Auth::user()->can('update-user'))
                        <div class="tab-pane" id="settings">

                            <form class="form-horizontal" role="form"
                                  action="{{route('users.update', ['id' => $user['id']])}}" method="POST"
                                  enctype="multipart/form-data">
                                {{csrf_field()}}


                                <input type="hidden" name="_method" value="put">
                                <div class="form-group @if($errors->has('name'))has-error @endif">
                                    <label for="name" class="col-sm-2 control-label">Имя</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="name" id="name" placeholder="Имя"
                                               value="{{$user['name']}}">
                                        @foreach ($errors->get('name') as $message)
                                            <span class="help-block">{{ $message }}</span>
                                        @endforeach
                                    </div>

                                </div>
                                <div class="form-group @if($errors->has('second_name'))has-error @endif">
                                    <label for="second_name" class="col-sm-2 control-label">Фамилия</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="second_name" id="second_name"
                                               placeholder="фамилия" value="{{$user['second_name']}}">
                                        @foreach ($errors->get('second_name') as $message)
                                            <span class="help-block">{{ $message }}</span>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group @if($errors->has('email'))has-error @endif">
                                    <label for="email" class="col-sm-2 control-label">Email</label>

                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" name="email" id="email"
                                               placeholder="email@email.com" value="{{$user['email']}}">
                                        @foreach ($errors->get('email') as $message)
                                            <span class="help-block">{{ $message }}</span>
                                        @endforeach
                                    </div>

                                </div>
                                <div class="form-group @if($errors->has('password'))has-error @endif">
                                    <label for="password" class="col-sm-2 control-label">Пароль</label>

                                    <div class="col-sm-10">
                                        <input type="password" class="form-control" name="password" id="password"
                                               placeholder="********" value="" minlength="6">
                                        @foreach ($errors->get('password') as $message)
                                            <span class="help-block">{{ $message }}</span>
                                        @endforeach
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="phone" class="col-sm-2 control-label">Номер телефона</label>

                                    <div class="col-sm-10">
                                        <the-mask mask="+# (###) ###-##-##"
                                                  @if($user['phone'])
                                                  :value="{{$user['phone']}}"
                                                  @endif
                                                  class="form-control"
                                                  placeholder="+7 (123) 456-78-99" name="phone" type="tel"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="phone" class="col-sm-2 control-label">Дата рождения</label>

                                    <div class="col-sm-10">
                                        <birthdate-component
                                                @if($user['birth_date'])
                                                :birth_date_user="'{{$user['birth_date']}}'"
                                                @endif
                                        ></birthdate-component>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="avatar" class="col-sm-2 control-label">Ваш Аватар</label>
                                    <div class="col-sm-10">
                                        <input type="file" name="avatar" id="avatar">
                                    </div>
                                </div>

                                @if(Auth::user()->can('assign-roles'))
                                    <div class="form-group">
                                        <label for="role" class="col-sm-2 control-label">Выберите
                                            роль:</label>
                                        <div class="col-sm-10">
                                            <roles-component :roles="{{$roles}}"
                                                             @if($rolesAssigned)
                                                             :user-roles="{!! $rolesAssigned !!}"
                                                    @endif
                                            ></roles-component>
                                        </div>
                                    </div>
                                @endif

                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox"> Я принимаю <a href="#">условия и правила</a>
                                                использования системой
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-danger">Изменить</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                @endif
                <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    <div class="modal modal-warning" id="deleteUser" tabindex="-1" role="dialog" aria-labelledby="deleteUser"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Удаление пользователя</h4>
                </div>
                <form role="form" action="{{route('users.destroy')}}" method="POST"
                      enctype="multipart/form-data"
                      id="formUserDelete">
                    <div class="modal-body">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="delete">
                        <input type="hidden" value="{{$user['id']}}" id="userID" name="id">

                        <p>Вы действительно хотите удалить пользователя <br/>
                            <span class="text-bold" id="userEmail">{{$user['email']}}</span> ?</p>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Отмена</button>
                        <button type="submit" class="btn btn-outline">Удалить</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal modal-warning" id="banUser" tabindex="-1" role="dialog" aria-labelledby="banUser"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Заблокировать пользователя</h4>
                </div>
                <form role="form" action="{{route('users.ban', ['id'=>$user['id']])}}" method="POST"
                      enctype="multipart/form-data"
                      id="formUserDelete">
                    <div class="modal-body">
                        {{csrf_field()}}
                        <input type="hidden" value="{{$user['id']}}" id="userID" name="id">

                        <p>Вы действительно хотите заблокировать пользователя <br/>
                            <span class="text-bold" id="userEmail">{{$user['email']}}</span> ?</p>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Отмена</button>
                        <button type="submit" class="btn btn-outline">Заблокировать</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal modal-warning" id="unbanUser" tabindex="-1" role="dialog" aria-labelledby="unbanUser"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Разблокировать пользователя</h4>
                </div>
                <form role="form" action="{{route('users.unBan', ['id'=>$user['id']])}}" method="POST"
                      enctype="multipart/form-data"
                      id="formUserDelete">
                    <div class="modal-body">
                        {{csrf_field()}}
                        <input type="hidden" value="{{$user['id']}}" id="userID" name="id">

                        <p>Вы действительно хотите разблокировать пользователя <br/>
                            <span class="text-bold" id="userEmail">{{$user['email']}}</span> ?</p>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Отмена</button>
                        <button type="submit" class="btn btn-outline">Разблокировать</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection


@section('scripts')
    <script>
        function deleteUser(id, email) {
            $("#userEmail").text(email);
            $("#userID").val(id);
        }
    </script>
@endsection