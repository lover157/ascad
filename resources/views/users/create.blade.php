@extends('layouts.app')
@section('pageTitle', 'Создание пользователя')

@section('content')

    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Создание пользователя</h3>
                </div>
                <div class="row">
                    <form role="form" action="{{route('users.store')}}" method="POST"
                          enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="box-body">
                            <div class="form-group @if($errors->has('email'))has-error @endif">
                                <label class="col-md-3 cols-sm-12 control-label">Email</label>
                                <div class="col-md-7 col-sm-12 el-input">
                                    <input type="email" name="email"
                                           class="form-control" placeholder="email@example.com">
                                    @foreach ($errors->get('email') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="role" class="col-md-3 sm-12 control-label">Выберите
                                    роль:</label>
                                <div class="col-md-9 col-sm-12">
                                    <roles-component :roles="{{$roles}}"></roles-component>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer" style="background: none">
                            <div class="form-group col-lg-2 col-lg-offset-8 col-sm-10 col-sm-offset-1 col-xs-12">
                                <button type="submit" class=" form-group btn input-block-level form-control btn-primary">
                                    Создать
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection