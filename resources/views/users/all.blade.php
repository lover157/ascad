@extends('layouts.app')
@section('pageTitle', 'Пользователи')

@section('content')
    <div class="row">
        <div class="col-xl-12 col-md-12 col-xs-12">
            @include('users.menu')
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Пользователи</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="hidden-xs">id</th>
                            <th>avatar</th>
                            <th>Имя</th>
                            <th class="hidden-xs">Email / телефон</th>
                            <th class="hidden-xs">Группы</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <a href="/users/">
                                <tr>
                                    <td class="hidden-xs">{{$user['id']}}</td>
                                    <td>
                                        @if($user['avatar'])
                                        <img src="/storage/avatars/{{$user['avatar']}}" class="img-circle
                                                @if(!$user['activated']) border-yellow @endif
                                                @if($user['banned']) border-red @endif"
                                                width="70px">
                                        @else
                                            <img src="{{ Avatar::create($user['name'].' '.$user['second_name'])->toBase64() }}" class="img-circle
                                            @if(!$user['activated']) border-yellow @endif
                                            @if($user['banned']) border-red @endif"
                                            width="70px"/>
                                        @endif
                                    </td>
                                    <td>{{$user['name']}} {{$user['second_name']}}</td>
                                    <td class="hidden-xs">
                                        <a href="mailto:{{$user['email']}}">{{$user['email']}}</a> <br/>
                                        @if($user['phone'])
                                            <a href="tel:{{$user['phone']}}">+{{$user['phone']}}</a>
                                        @endif
                                    </td>
                                    <td class="hidden-xs">
                                        <?php $userRoles = $user->roles?>
                                        @foreach($userRoles as $role)
                                            {{$role->display_name}} <br/>
                                        @endforeach
                                    </td>
                                    <td>
                                        <a href="{{route('users.show', ['id' => $user['id']])}}"
                                           aria-hidden="true"><i
                                                    class="fa fa-eye fa-2x" aria-hidden="true"
                                                    title="Просмотреть"></i>
                                        </a>
                                        <a href="#"
                                           data-toggle="modal" data-target="#deleteUser"
                                           onclick="deleteUser('{{$user["id"]}}', '{{$user['email']}}')"
                                        ><i
                                                    class="fa fa-trash fa-2x text-yellow" aria-hidden="true"
                                                    title="Удалить"></i></a>
                                    </td>
                                </tr>
                            </a>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th class="hidden-xs">id</th>
                            <th>avatar</th>
                            <th>Имя</th>
                            <th class="hidden-xs">Email / телефон</th>
                            <th class="hidden-xs">Группы</th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>

                </div>
                <div class="box-footer text-center">
                    {{--{{ $users->links() }}--}}
                </div>
                <!-- /.box-body -->
            </div>

        </div>
    </div>

    <div class="modal modal-warning" id="deleteUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Удаление пользователя</h4>
                </div>
                <form role="form" action="{{route('users.destroy')}}" method="POST"
                      enctype="multipart/form-data"
                      id="formUserDelete">
                    <div class="modal-body">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="delete">
                        <input type="hidden" value="" id="userID" name="id">

                        <p>Вы действительно хотите удалить пользователя <br/>
                            <span class="text-bold" id="userEmail"></span> ?</p>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Отмена</button>
                        <button type="submit" class="btn btn-outline">Удалить пользователя</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection


@section('scripts')
    <script>
        function deleteUser(id, email) {
            $("#userEmail").text(email);
            $("#userID").val(id);
        }
    </script>
@endsection