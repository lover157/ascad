@extends('layouts.app')
@section('pageTitle', 'Клиент: '. $client->name. ' '. $client->second_name)

@section('content')

    <div class="row">
        <div class="col-lg-8">
            <div class="box">
                <div class="box-header"><h4>Информация о клиенте:</h4></div>
                <div class="box-body">
                    <p>ФИО: {{$client->second_name}} {{$client->name}} {{$client->third_name}}</p>

                    <p>Телефон 1: +{{$client->phone1}}</p>
                    @if($client->phone2)
                        <p>Телефон 2: +{{$client->phone2}}</p>
                    @endif
                    @if($client->email)
                        <p>email: <a href="mailto:{{$client->email}}">{{$client->email}}</a></p>
                    @endif
                    @if($client->comment)
                        <p>Комментарий: {{$client->comment}}</p>
                    @endif
                </div>
                <div class="box-footer">
                    <h4>Адреса:</h4>
                    <div class="row addresses">
                        @foreach($client->addresses as $address)
                            <div class="col-md-4">
                                <div class="address-box">
                                    <p id="locality_{{$address->id}}">{{$address['locality']}}</p>
                                    <p id="street_{{$address->id}}">{{$address['street']}}</p>
                                    <p id="object_number_{{$address->id}}">{{$address['object_number']}}</p>
                                    <p class="" style="position: absolute; bottom: 5px;">
                                        <a data-toggle="modal" data-target="#updateAddress" onclick="editAddress('{{$address->id}}')"><i class="fa fa-2x fa-pencil"></i></a>
                                        <a data-toggle="modal" data-target="#deleteAddress" onclick="deleteAddress('{{$address->id}}')"><i class="fa fa-2x fa-trash"></i></a>
                                    </p>
                                </div>
                            </div>
                        @endforeach
                        <div class="col-md-4 text-center address-add">

                            <a class="address-add-button" data-toggle="modal" data-target="#createAddress"> <i
                                        class="fa fa-3x fa-plus fa-plus-rotate-hover"></i></a>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <h4>Заказы:</h4>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="box">
                <div class="box-header"><h4>Общая информация:</h4></div>
                <div class="box-body">
                    @if($client->dealer_id)
                        <p>Дилер:
                            <a href="{{route('dealers.show', ['id' => $client->dealer['id']])}}">{{$client->dealer['name']}} {{$client->dealer['second_name']}}</a>
                        </p>
                    @endif
                    @if($client->user_id)
                        <p>Создал:
                            <a href="{{route('users.show', ['id' => $client->user['id']])}}">{{$client->user['name']}} {{$client->user['second_name']}}</a>
                        </p>
                    @endif
                    <p>Дата создания: {{$client->created_at}}</p>
                    <p>Дата изменения: {{$client->updated_at}}</p>
                </div>
                <div class="box-footer">
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-success" id="createAddress" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Создать адрес</h4>
                </div>
                <form role="form" action="{{route('addresses.store')}}" method="POST"
                      enctype="multipart/form-data"
                      id="createAddress">
                    <div class="modal-body">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="post">
                        <input type="hidden" name="client_id" value="{{$client->id}}">
                        <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                        <div class="form-group @if($errors->has('locality'))has-error @endif">
                            <label for="locality">Нас. пункт</label>
                            <input type="text" class="form-control" placeholder="Район / нас. пункт" name="locality"
                                   id="locality"
                                   value="{{Input::old('locality')}}"
                                   required>
                            @foreach ($errors->get('locality') as $message)
                                <span class="help-block">{{ $message }}</span>
                            @endforeach
                        </div>
                        <div class="form-group @if($errors->has('street'))has-error @endif">
                            <label for="street">Улица</label>
                            <input type="text" class="form-control" placeholder="Улица" name="street" id="street"
                                   value="{{Input::old('street')}}"
                                   required>
                            @foreach ($errors->get('street') as $message)
                                <span class="help-block">{{ $message }}</span>
                            @endforeach
                        </div>
                        <div class="form-group @if($errors->has('object_number'))has-error @endif">
                            <label for="object_number">Кадастровый номер объекта</label>
                            <input type="text" class="form-control" placeholder="####" name="object_number" id="object_number"
                                   value="{{Input::old('object_number')}}">
                            @foreach ($errors->get('object_number') as $message)
                                <span class="help-block">{{ $message }}</span>
                            @endforeach
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Отмена</button>
                        <button type="submit" class="btn btn-outline">Создать</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal modal-warning" id="updateAddress" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Изменить адрес</h4>
                </div>
                <form role="form" method="POST" enctype="multipart/form-data" id="updateAddress" action="{{route('addresses.update')}}">
                    <div class="modal-body">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="put">
                        <input type="hidden" value="" name="id" id="address_id">
                        <input type="hidden" name="client_id" value="{{$client->id}}">
                        <div class="form-group @if($errors->has('locality_upd'))has-error @endif">
                            <label for="locality_upd">Нас. пункт</label>
                            <input type="text" class="form-control" placeholder="Район / нас. пункт" name="locality_upd"
                                   id="locality_upd"
                                   value="{{Input::old('locality_upd')}}"
                                   required>
                            @foreach ($errors->get('locality_upd') as $message)
                                <span class="help-block">{{ $message }}</span>
                            @endforeach
                        </div>
                        <div class="form-group @if($errors->has('street_upd'))has-error @endif">
                            <label for="street_upd">Улица</label>
                            <input type="text" class="form-control" placeholder="Улица" name="street_upd" id="street_upd"
                                   value="{{Input::old('street_upd')}}"
                                   required>
                            @foreach ($errors->get('street_upd') as $message)
                                <span class="help-block">{{ $message }}</span>
                            @endforeach
                        </div>
                        <div class="form-group @if($errors->has('numbers'))has-error @endif">
                            <label for="object_number_upd">Кадастровый номер объекта</label>
                            <input type="text" class="form-control" placeholder="14, кв 1" name="object_number_upd" id="object_number_upd"
                                   value="{{Input::old('object_number_upd')}}"
                                   required>
                            @foreach ($errors->get('numbers') as $message)
                                <span class="help-block">{{ $message }}</span>
                            @endforeach
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Отмена</button>
                        <button type="submit" class="btn btn-outline">Изменить</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal modal-danger" id="deleteAddress" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Удалить адрес</h4>
                </div>
                <form role="form" method="POST" enctype="multipart/form-data" action="{{route('addresses.destroy')}}">
                    <div class="modal-body">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="delete">
                        <input type="hidden" value="" name="id" id="del_address_id">
                        <input type="hidden" name="client_id" value="{{$client->id}}">
                        <p>Вы действительно хотите удалить адрес ?</p>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Отмена</button>
                        <button type="submit" class="btn btn-outline">Удалить</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        function editAddress(id) {
            var locality = $("#locality_" + id).text();
            var street = $("#street_" + id).text();
            var numbers = $("#object_number_" + id).text();

            $("#address_id").val(id);
            $("#locality_upd").val(locality);
            $("#street_upd").val(street);
            $("#object_number_upd").val(numbers);
        }
        function deleteAddress(id) {
            $("#del_address_id").val(id);
        }
    </script>
@endsection