@extends('layouts.app')
@section('pageTitle', 'Создание нового клиента')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <form role="form" action="{{route('clients.update', ['id'=> $client->id])}}" method="POST"
                  enctype="multipart/form-data">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="put">
                <div class="box-body">
                    <div class="form-group @if($errors->has('name'))has-error @endif">
                        <label for="name">Имя</label>
                        <input type="text" class="form-control" placeholder="Имя" name="name" id="name"
                               value="{{Input::old('name') ? Input::old('name') : $client->name}}"
                               required>
                        @foreach ($errors->get('name') as $message)
                            <span class="help-block">{{ $message }}</span>
                        @endforeach

                    </div>
                    <div class="form-group @if($errors->has('second_name'))has-error @endif">
                        <label for="second_name">Фамилия</label>
                        <input type="text" class="form-control" placeholder="Фамилия" name="second_name"
                               id="second_name"
                               value="{{Input::old('second_name') ? Input::old('second_name') : $client->second_name}}"
                               required>
                        @foreach ($errors->get('second_name') as $message)
                            <span class="help-block">{{ $message }}</span>
                        @endforeach
                    </div>
                    <div class="form-group @if($errors->has('third_name'))has-error @endif">
                        <label for="third_name">Отчество</label>
                        <input type="text" class="form-control" placeholder="Отчество" name="third_name" id="third_name"
                               value="{{Input::old('third_name') ? Input::old('third_name') : $client->third_name}}"
                               required>
                        @foreach ($errors->get('third_name') as $message)
                            <span class="help-block">{{ $message }}</span>
                        @endforeach
                    </div>
                    <div class="form-group @if($errors->has('email'))has-error @endif">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" placeholder="email@example.com" name="email" id="email"
                               value="{{Input::old('email') ? Input::old('email') : $client->email}}"
                        >
                        @foreach ($errors->get('email') as $message)
                            <span class="help-block">{{ $message }}</span>
                        @endforeach
                    </div>

                    <div class="form-group">
                        <label for="phone" class="col-sm-2 control-label">Номер телефона 1</label>
                        <div class="col-sm-10">
                            <the-mask mask="+# (###) ###-##-##"
                                      :value="{{Input::old('phone1') ? Input::old('phone1') : $client->phone1}}"
                                      class="form-control"
                                      placeholder="+7 (123) 456-78-99" name="phone1" type="tel">

                            </the-mask>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="phone" class="col-sm-2 control-label">Номер телефона 2</label>
                        <div class="col-sm-10">
                            <the-mask mask="+# (###) ###-##-##"
                                      :value="{{Input::old('phone2') ? Input::old('phone2') : $client->phone2}}"
                                      class="form-control"
                                      placeholder="+7 (123) 456-78-99" name="phone2" type="tel"></the-mask>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="comment">Комментарий</label>
                        <textarea name="comment" class="form-control" placeholder="Комментарий к клиенту" rows="3"
                                  id="comment"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="dealer" class="col-sm-2 control-label">Выберите Дилера</label>
                        <div class="col-md-7 col-sm-12 el-input">
                            <select name="dealer_id" class="form-control" id="dealer">
                                <option value=""> Выбор Дилера</option>
                                @foreach($dealers as $dealer)
                                    <option value="{{$dealer->id}}"
                                            @if($client->dealer_id == $dealer->id) selected @endif> {{$dealer->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                </div>
                <!-- /.box-body -->

                <div class="box-footer text-right" style="background: none">
                    <button type="submit" class="btn btn-primary">Изменить</button>
                </div>
            </form>
        </div>
    </div>
@endsection