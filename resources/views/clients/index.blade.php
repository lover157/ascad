@extends('layouts.app')
@section('pageTitle', 'Клиенты')

@section('content')
    <div class="row">
        <div class="col-xl-12 col-md-12 col-xs-12">
            @include('clients.menu')

            <div class="box box-info">
                <div class="box-header">
                    <h1 class="box-title">Клиенты</h1>
                </div>

                <div class="box-body">
                    <div class="row">
                        @foreach($clients as $client)
                            <div class="col-md-4">
                                <div class="box">
                                    <div class="box-header">
                                        <div class="box-title">
                                            <img src="{{ Avatar::create($client->name.' '. $client->second_name)->toBase64() }}"
                                                 width="30px"/>
                                            <a href="{{route('clients.show',['id'=>$client->id])}}">{{$client->name}} {{ $client->second_name}}</a>
                                            <button type="button" class="btn btn-sm dropdown-toggle btn-actions"
                                                    data-toggle="dropdown" aria-expanded="false">
                                                <i class="fa fa-cogs"></i>
                                                <span class="fa fa-caret-down"></span></button>
                                            <ul class="dropdown-menu dropdown-actions">
                                                <li><a href="{{route('clients.edit', ['id'=>$client->id])}}"><i
                                                                class="fa fa-pencil"></i>Изменить</a></li>
                                                <li><a href="#"  data-toggle="modal" data-target="#deleteClient"
                                                       onclick="deleteClient('{{$client->id}}', '{{$client->name}} {{$client->second_name}}')"><i class="fa fa-trash"></i>Удалить</a></li>

                                                <li class="divider"></li>
                                                <li><a href="#">Создать заказ</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Адреса</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        @if($client->email)
                                            email :
                                            <a href="mailto: {{ $client->email}}">
                                                {{ $client->email}}
                                            </a>
                                            <br/>
                                        @endif
                                        @if($client->phone1)
                                            телефон :
                                            <a href="tel:{{ $client->phone1}}">
                                                {{ $client->phone1}}
                                            </a>
                                            <br/>
                                        @endif
                                        @if($client->phone2)
                                            телефон :
                                            <a href="tel:{{ $client->phone2}}">
                                                {{ $client->phone2}}
                                            </a>
                                            <br/>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal modal-warning" id="deleteClient" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Удаление клиента</h4>
                </div>
                <form role="form" action="{{route('clients.destroy')}}" method="POST"
                      enctype="multipart/form-data"
                      id="formUserDelete">
                    <div class="modal-body">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="delete">
                        <input type="hidden" value="" id="clientID" name="id">

                        <p>Вы действительно хотите удалить клиента <br/>
                            <span class="text-bold" id="clientName"></span> ?</p>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Отмена</button>
                        <button type="submit" class="btn btn-outline">Удалить клиента</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection


@section('scripts')
    <script>
        function deleteClient(id, name) {
            $("#clientName").text(name);
            $("#clientID").val(id);
        }
    </script>
@endsection