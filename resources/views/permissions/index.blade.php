@extends('layouts.app')
@section('pageTitle', 'Права (действия)')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Права (действия)</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>Наименование</th>
                            <th>Название</th>
                            <th>Описание</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($permissions as $permission)
                            <tr>
                                <td>{{$permission['id']}}</td>
                                <td class="hidden-xs">{{$permission['name']}}</td>
                                <td>{{$permission['display_name']}}</td>
                                <td class="hidden-xs">{{$permission['description']}}</td>
                                <td>
                                    <a href="{{route('permissions.edit', ['id' => $permission['id']])}}"><i
                                                class="fa fa-pencil-square fa-2x" aria-hidden="true"
                                                title="Редактировать"></i></a>
                                    <a href="{{route('permissions.destroy', ['id' => $permission['id']])}}"><i
                                                class="fa fa-trash fa-2x" aria-hidden="true"
                                                title="Удалить"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>id</th>
                            <th>Наименование</th>
                            <th>Название</th>
                            <th>Описание</th>
                            <th>Действия</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

@endsection