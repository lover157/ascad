@extends('layouts.app')
@section('pageTitle', 'Создание нового права')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <form role="form" action="{{route('permissions.update', ['id'=>$permission->id])}}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                {{method_field('put')}}
                <div class="box-body">
                    <div class="form-group">
                        <label for="name">Наименование (на англ без пробелов)</label>
                        <input type="text" class="form-control" placeholder="Название права " name="name" id="name" value="{{$permission->name}}">
                    </div>
                    <div class="form-group">
                        <label for="display_name">Название</label>
                        <input type="text" class="form-control" placeholder="Название" name="display_name" value="{{$permission->display_name}}"
                               id="display_name">
                    </div>
                    <div class="form-group">
                        <label for="description">Описание</label>
                        <textarea name="description" class="form-control" placeholder="Описание права" rows="3"
                                  id="description">{{$permission->description}}</textarea>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer text-right" style="background: none">
                    <button type="submit" class="btn btn-primary">Изменить</button>
                </div>
            </form>
        </div>
    </div>
@endsection