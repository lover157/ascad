@extends('layouts.app')
@section('pageTitle', 'Виды работ')

@section('content')

    <?php
    $traverse = function ($services, $prefix = '') use (&$traverse) {
        foreach ($services as $service) {
            echo PHP_EOL . '
                <tr>
                    <td>'.$service->id.'</td>
                    <td>'.$prefix . ' ' . $service->name . '</td>
                    <td>'.$service->description.'</td>
                    <td>
                        <a href="'.route('services.edit', ['id' => $service['id']]).'" aria-hidden="true"><i class="fa fa-pencil fa-2x" aria-hidden="true" title="Просмотреть"></i></a>
                        <a href="#" data-toggle="modal" data-target="#deleteService"
                        onclick="deleteService(\''.$service['id'] .'\' , \''.$service['name'].'\' )">
                        <i class="fa fa-trash fa-2x text-yellow" aria-hidden="true" title="Удалить"></i></a>
                    </td>
                </tr>';

            $traverse($service->children, $prefix . '<i class="fa fa-share fa-flip-vertical"></i>');
        }
    };
    ?>

    <div class="row">
        <div class="col-xs-12">
            @include('settings.services.menu')
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Виды работ</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>Название</th>
                            <th>Описание</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {{$traverse($services)}}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>id</th>
                            <th>Название</th>
                            <th>Описание</th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>


    <div class="modal modal-warning" id="deleteService" tabindex="-1" role="dialog" aria-labelledby="modaldeleteService"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Удаление пользователя</h4>
                </div>
                <form role="form" action="{{route('services.destroy')}}" method="POST"
                      enctype="multipart/form-data"
                      id="formUserDelete">
                    <div class="modal-body">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="delete">
                        <input type="hidden" value="" id="serviceID" name="id">

                        <p>Вы действительно хотите удалить Работу <br/>
                            <span class="text-bold" id="serviceName"></span> ?</p>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Отмена</button>
                        <button type="submit" class="btn btn-outline">Удалить работу</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('scripts')
    <script>
        function deleteService(id, name) {
            $("#serviceName").text(name);
            $("#serviceID").val(id);
        }
    </script>
@endsection