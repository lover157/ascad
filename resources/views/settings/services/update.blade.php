@extends('layouts.app')
@section('pageTitle', 'Изменение услуги')

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Изменение вида работ</h3>
                </div>
                <div class="row">
                    <form role="form" action="{{route('services.update', ['id'=>$service->id])}}" method="POST">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="put">
                        <div class="box-body">
                            <div class="form-group @if($errors->has('name'))has-error @endif">
                                <label for="name" class="col-md-3 cols-sm-12 control-label">Название</label>
                                <div class="col-md-7 col-sm-12 el-input">
                                    <input type="text" name="name" id="name"
                                           class="form-control" placeholder="Услуга" value="{{$service->name}}">
                                    @foreach ($errors->get('name') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('description'))has-error @endif">
                                <label for="description" class="col-md-3 cols-sm-12 control-label">Описание</label>
                                <div class="col-md-7 col-sm-12 el-input">
                                    <input type="text" name="description"
                                           class="form-control" placeholder="Описание услуги" id="description" value="{{$service->description}}">
                                    @foreach ($errors->get('description') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="box-footer" style="background: none">
                            <div class="form-group col-lg-2 col-lg-offset-8 col-sm-10 col-sm-offset-1 col-xs-12">
                                <button type="submit"
                                        class=" form-group btn input-block-level form-control btn-primary">
                                    Изменить
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

@endsection

