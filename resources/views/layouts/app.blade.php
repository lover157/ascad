<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('pageTitle') - ASCAD.PRO</title>
    <!-- Styles -->
    <!-- Theme style -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/AdminLTE.css') }}" rel="stylesheet">
    <link href="{{ asset('css/themes.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
</head>

<body class="skin-blue sidebar-mini {{\Illuminate\Support\Facades\Cookie::get('sidebar-menu')}}">
<div class="wrapper" id="app">

    @include('layouts.app_parts.header')
    @auth
    @include('layouts.app_parts.sidebar')
    @endauth
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            @yield('content')
        </section>
        <!-- /.content -->
    </div>

    @include('layouts.app_parts.footer')
</div>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('js/scripts.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('js/demo.js') }}"></script>
@yield('scripts')
{{--@if(Session::has('user_created'))--}}
    {{--<script>--}}
        {{--let UserCreated = Vue.extend({--}}
            {{--created() {--}}
                {{--this.$notify({--}}
                    {{--title: 'Пользователь создан',--}}
                    {{--message: '{{Session::get('user_created')}}',--}}
                    {{--type: 'success',--}}
                {{--});--}}
            {{--}--}}
        {{--});--}}
        {{--new UserCreated().$mount('#mount-point');--}}
    {{--</script>--}}
{{--@endif--}}
@include('layouts.app_parts.notifications')
</body>
</html>
