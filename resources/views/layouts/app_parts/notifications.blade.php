@if(Session::has('notification'))
    <?php
        $data = Session::get('notification')
    ?>
    <script>
        let Notification = Vue.extend({
            created() {
                this.$notify({
                    title: '{{$data['title']}}',
                    message: '{{$data['message']}}',
                    type: '{{$data['type']}}',
                });
            }
        });
        new Notification().$mount('#mount-point');
    </script>
@endif