<div class="nbs">
    <a href="{{route('users.create')}}" class="nb nbi nbsb1">П <span>Создать пользователя</span></a>
    <a href="{{route('dealers.create')}}" class="nb nbi nbsb2">Д <span>Создать дилера</span></a>
    <a href="{{route('clients.create')}}" class="nb nbi nbsb3">К <span>Создать клиента</span></a>
    <a href="{{route('orders.create')}}" class="nb nbi nbsb4">З <span>Создать заказ</span></a>
    <a class="nb nb-main"> + <span>Создать</span></a>
</div>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>ASCad</b> 2.0.0
    </div>
    <strong>Copyright &copy; 2017.</strong> All rights reserved.
</footer>