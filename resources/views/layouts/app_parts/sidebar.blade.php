<aside class="main-sidebar ">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->

        <div class="user-panel">
            <div class="pull-left image">
                <img src="/storage/avatars/{{ Auth::user()->avatar }}" class="img-circle">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }} {{ Auth::user()->second_name }}</p>
                <a href="#">
                    {{--@if(Auth::user()->isOnline())--}}
                        <i class="fa fa-circle text-success"></i> Online</a>
                    {{--@endif--}}
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Поиск...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">Меню</li>
            <li class="{{ isActiveURL('/', 'active')}}">
                <a href="/">
                    <i class="fa fa-dashboard"></i> <span>Панель</span>
                    <span class="pull-right-container">
            </span>
                </a>
            </li>
            <li class="treeview {{ areActiveRoutes(['dealers.*', 'clients.*'], 'active')}}">
                <a href="#">
                    <i class="fa fa-files-o text-red"></i>
                    <span>Органайзер</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ areActiveRoutes(['clients.*'], 'active')}}">
                        <a href="{{route('clients.index')}}"><i class="fa fa-circle-o"></i> Клиенты</a>
                    </li>
                    <li class="{{ areActiveRoutes(['dealers.*'], 'active')}}"><a href="{{route('dealers.index')}}"><i class="fa fa-circle-o"></i> Дилеры</a></li>
                    <li><a href="#"><i class="fa fa-calendar"></i> Календарь</a></li>
                    <li><a href="#"><i class="fa fa-edit"></i> Заметки</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-files-o text-red"></i>
                    <span>Отчеты</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/"><i class="fa fa-pie-chart"></i> Статистика</a></li>
                </ul>
            </li>
            <li class="treeview {{ areActiveRoutes(['orders.*'], 'active')}}">
                <a href="#">
                    <i class="fa fa-files-o text-red"></i>
                    <span>Заказы</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('orders.index')}}"><i class="fa fa-pie-chart"></i> Все</a></li>
                    <li><a href="/"><i class="fa fa-pie-chart"></i> В работе</a></li>
                    <li><a href="/"><i class="fa fa-pie-chart"></i> Остановка</a></li>
                    <li><a href="/"><i class="fa fa-pie-chart"></i> Архив</a></li>
                </ul>
            </li>

            <li class="treeview {{ areActiveRoutes(['roles.*', 'users.*', 'permissions.*'], 'active')}}">
                <a href="#">
                    <i class="fa fa-users text-yellow"></i>
                    <span>Пользователи и группы</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ areActiveRoutes(['users.*'], 'active')}}"><a href="{{route('users.index')}}"><i class="fa fa-user"></i> Пользователи</a></li>
                    <li class="{{ areActiveRoutes(['roles.*'], 'active')}}"><a href="{{route('roles.index')}}"><i class="fa fa-users"></i> Группы</a></li>
                    @role('superadmin')
                    <li class="{{ areActiveRoutes(['permissions.*'], 'active')}}"><a href="{{route('permissions.index')}}"><i class="fa fa-gamepad"></i> Права</a></li>
                    @endrole
                </ul>
            </li>
            @role('superadmin|admin')
            <li class="treeview {{ areActiveRoutes(['settings.*'], 'active')}}" >
                <a href="#">
                    <i class="fa fa-cogs text-yellow"></i>
                    <span>Настройки</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-lock"></i> Основные</a></li>
                    <li class="{{ areActiveRoutes(['services.*'], 'active')}}">
                        <a href="{{route('services.index')}}"><i class="fa fa-lock"></i> Услуги (виды работ)</a>
                    </li>
                </ul>
            </li>
            @endrole
        </ul>

    </section>
    <!-- /.sidebar -->
</aside>