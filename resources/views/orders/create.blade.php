@extends('layouts.app')
@section('pageTitle', 'Заказы')

@section('content')

    <div class="row">

        <div class="col-xl-12 col-md-12 col-xs-12">
            <div class="box box-default">
                <div class="box-header">
                    <h1 class="box-title">Создание заказа</h1>
                </div>
                <form role="form" action="{{route('orders.store')}}" method="POST"
                      enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-md-2 col-sm-6">
                                <input type="radio" name="client_type" id="new_client" checked value="new_client"/>
                                <label for="new_client">Новый клиент</label>
                            </div>
                            <div class="col-md-2 col-sm-6">
                                <input type="radio" name="client_type" id="old_client" value="old_client"/>
                                <label for="old_client">Выбрать клиента</label>
                            </div>
                        </div>
                        <div class="new-client ">
                            <div class="form-group @if($errors->has('name'))has-error @endif">
                                <label for="name" class="col-md-2 cols-sm-12 control-label">Имя</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="text" name="name" id="name"
                                           value="{{Input::old('name')}}"
                                           class="form-control" placeholder="John" required>
                                    @foreach ($errors->get('name') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('name'))has-error @endif">
                                <label for="name" class="col-md-2 cols-sm-12 control-label">Фамилия</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="text" name="name" id="name"
                                           value="{{Input::old('name')}}"
                                           class="form-control" placeholder="Doe" required>
                                    @foreach ($errors->get('name') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('name'))has-error @endif">
                                <label for="name" class="col-md-2 cols-sm-12 control-label">Отчество</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="text" name="name" id="name"
                                           value="{{Input::old('name')}}"
                                           class="form-control" placeholder="John " required>
                                    @foreach ($errors->get('name') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('email'))has-error @endif">
                                <label for="email" class="col-md-2 cols-sm-12 control-label">Email</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="email" name="email" id="email"
                                           value="{{Input::old('email')}}"
                                           class="form-control" placeholder="JohnDoe@example.com" required>
                                    @foreach ($errors->get('email') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="phone" class="col-sm-2 control-label">Номер телефона 1</label>
                                <div class="col-md-7 col-sm-12">
                                    <the-mask mask="+# (###) ###-##-##"
                                              @if(Input::old('phone1'))
                                              :value="{{Input::old('phone1')}}"
                                              @endif
                                              class="form-control"
                                              placeholder="+7 (123) 456-78-99" name="phone1" type="tel">

                                    </the-mask>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="phone" class="col-sm-2 control-label">Номер телефона 2</label>
                                <div class="col-md-7 col-sm-12">
                                    <the-mask mask="+# (###) ###-##-##"
                                              @if(Input::old('phone2'))
                                              :value="{{Input::old('phone2')}}"
                                              @endif
                                              class="form-control"
                                              placeholder="+7 (123) 456-78-99" name="phone2" type="tel"></the-mask>
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('locality'))has-error @endif">
                                <label for="locality" class="col-md-2 cols-sm-12 control-label">Район / город</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="email" name="locality" id="locality"
                                           value="{{Input::old('locality')}}"
                                           class="form-control" placeholder="г. Симферополь" required>
                                    @foreach ($errors->get('locality') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('street'))has-error @endif">
                                <label for="street" class="col-md-2 cols-sm-12 control-label">Улица</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="email" name="street" id="street"
                                           value="{{Input::old('street')}}"
                                           class="form-control" placeholder="ул. Ленина" required>
                                    @foreach ($errors->get('street') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-numbers @if($errors->has('numbers'))has-error @endif">
                                <label for="numbers" class="col-md-2 cols-sm-12 control-label">Номера (дом, квартира)</label>
                                <div class="col-md-7 col-sm-12">
                                    <input type="text" name="numbers" id="numbers"
                                           value="{{Input::old('numbers')}}"
                                           class="form-control" placeholder="д. 36, кв. 12" required>
                                    @foreach ($errors->get('numbers') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="old-client hidden">
                            <div class="form-group">
                                <label for="client_id" class="col-md-2 cols-sm-12 control-label">Клиент</label>
                                <div class="col-md-7 col-sm-12">
                                    <select class="form-control" name="client_id" required id="client_id">
                                        @foreach($clients as $client)
                                        <option value="{{$client->id}}">{{$client->name}} {{$client->second_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="address_id" class="col-md-2 cols-sm-12 control-label">Адрес</label>
                                <div class="col-md-7 col-sm-12">
                                    <select class="form-control" name="address_id" required id="address_id" onchange="selectAddress()">
                                        <option value="0">Выбор адреса</option>
                                        @foreach($clients as $client)
                                            <option value="{{$client->id}}">{{$client->name}} {{$client->second_name}}</option>
                                        @endforeach
                                        <option value="new">Новый адрес</option>
                                    </select>
                                </div>
                            </div>

                            <div id="new-address" class="hidden">
                                <div class="form-group @if($errors->has('locality'))has-error @endif">
                                    <label for="locality" class="col-md-2 cols-sm-12 control-label">Район / город</label>
                                    <div class="col-md-7 col-sm-12">
                                        <input type="email" name="new_locality" id="new_locality"
                                               value="{{Input::old('locality')}}"
                                               class="form-control" placeholder="г. Симферополь" required>
                                        @foreach ($errors->get('locality') as $message)
                                            <span class="help-block">{{ $message }}</span>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group @if($errors->has('street'))has-error @endif">
                                    <label for="street" class="col-md-2 cols-sm-12 control-label">Улица</label>
                                    <div class="col-md-7 col-sm-12">
                                        <input type="email" name="new_street" id="new_street"
                                               value="{{Input::old('street')}}"
                                               class="form-control" placeholder="ул. Ленина" required>
                                        @foreach ($errors->get('street') as $message)
                                            <span class="help-block">{{ $message }}</span>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-numbers @if($errors->has('numbers'))has-error @endif">
                                    <label for="new_numbers" class="col-md-2 cols-sm-12 control-label">Номера (дом, квартира)</label>
                                    <div class="col-md-7 col-sm-12">
                                        <input type="text" name="new_numbers" id="new_numbers"
                                               value="{{Input::old('numbers')}}"
                                               class="form-control" placeholder="д. 36, кв. 12" required>
                                        @foreach ($errors->get('numbers') as $message)
                                            <span class="help-block">{{ $message }}</span>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                </form>
                <div class="box-footer">

                </div>
            </div>
        </div>

    </div>



@endsection


@section('scripts')
    <script type="text/javascript">
        $('[name=client_type]').change(function () {
            if (this.value == 'new_client') {
                $('.new-client').removeClass('hidden');
                $('.old-client').addClass('hidden');

                $("#name").prop('required', true);
                $("#email").prop('required', true);
                $("#phone").prop('required', true);

            }
            else if (this.value == 'old_client') {
                $('.old-client').removeClass('hidden');
                $('.new-client').addClass('hidden');

                $("#name").prop('required', false);
                $("#email").prop('required', false);
                $("#phone").prop('required', false);
            }
        });

        function selectAddress() {
            var address_id = $('#address_id').val();
            if(address_id == 'new'){
                $('#new-address').removeClass('hidden')
            } else{
                $('#new-address').addClass('hidden')

            }
        }
    </script>
@endsection