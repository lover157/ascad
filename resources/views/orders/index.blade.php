@extends('layouts.app')
@section('pageTitle', 'Заказы')

@section('content')
    <div class="row">
        <div class="col-xs-6">
            <a href="{{route('orders.create')}}" class="btn btn-success" style="margin-bottom: 10px">
                <i class="fa fa-plus" aria-hidden="true" title="Add new Order"></i> Создать заказ</a>
            <a href="#filtertoogle" class="btn btn-primary" style="margin-bottom: 10px" data-toggle="collapse"
               role="button"
               aria-expanded="false" aria-controls="filtertoogle">
                <i class="fa fa-search" aria-hidden="true" title="Show Filter"></i> Фильтр</a>
        </div>
    </div>
    <?php
    $collapse = 'collapse';
    if (Request::get('filter') == 'y')
        $collapse = ''
    ?>
    <div class="row {{$collapse}}" id="filtertoogle">
        <div class="col-xs-12">
            <form action="" method="GET">
                <input type="hidden" value="y" name="filter">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Filter:</h3>
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4 col-sm-12">
                                <h4>Dates</h4>
                            </div>

                            <div class="col-md-4 col-sm-12">
                                <h4>Routes</h4>

                            </div>

                            <div class="col-md-4 col-sm-12">
                                <h4>Other</h4>

                            </div>


                        </div>
                    </div>

                    <div class="box-footer">
                        <a href="{{route('orders.index')}}"
                           class="col-md-3 col-md-offset-2 col-xs-12 btn btn-danger"
                        >Reset</a>
                        <input type="submit" class="col-md-3 col-md-offset-2 col-xs-12 btn btn-primary"
                               value="Filter"/>
                    </div>

                </div>
            </form>
        </div>
    </div>
    <div class="row">
        @foreach($orders as $order)
            <div class="col-xl-12 col-md-12 col-xs-12">
                <div class="box"  style=" border: 2px solid #d9534f;">
                    <div class="box-header">
                        <h1 class="box-title">{{$order->id}}</h1>
                    </div>

                    <div class="box-body">
                        <div class="row">

                            <div class="col-md-4">
                                <p>
                                    {{$order->client['name']}} {{$order->client['second_name']}}
                                </p>
                                <p>
                                    {{$order->client['phone1']}}
                                </p>
                                <p>
                                    {{$order->client['phone2']}}
                                </p>
                                <p>
                                    {{$order->address['locality']}}, {{$order->address['street']}}, {{$order->address['numbers']}}
                                </p>
                            </div>
                            <div class="col-md-4">

                            </div>
                            <div class="col-md-4"></div>

                        </div>
                    </div>

                    <div class="box-footer">

                    </div>
                </div>
            </div>
        @endforeach
    </div>



@endsection


@section('scripts')

@endsection