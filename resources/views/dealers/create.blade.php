@extends('layouts.app')
@section('pageTitle', 'Добавление нового дилера')

@section('content')
    <div class="row">
        <div class="col-xl-12 col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Добавление нового дилера</h3>
                </div>
                <div class="row">
                    <form role="form" action="{{route('dealers.store')}}" method="POST"
                          enctype="multipart/form-data">
                        {{csrf_field()}}

                        <div class="box-body">
                            <div class="form-group @if($errors->has('name'))has-error @endif">
                                <label for="name" class="col-md-3 cols-sm-12 control-label">Имя</label>
                                <div class="col-md-7 col-sm-12 el-input">
                                    <input type="text" name="name" id="name"
                                           value = "{{Input::old('name')}}"
                                           class="form-control" placeholder="Имя">
                                    @foreach ($errors->get('name') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('second_name'))has-error @endif">
                                <label for="second_name" class="col-md-3 cols-sm-12 control-label">Фамилия</label>
                                <div class="col-md-7 col-sm-12 el-input">
                                    <input type="text" name="second_name" id="second_name"
                                           value = "{{Input::old('second_name')}}"
                                           class="form-control" placeholder="Фамилия">
                                    @foreach ($errors->get('second_name') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('phone'))has-error @endif">
                                <label for="name" class="col-md-3 cols-sm-12 control-label">Телефон</label>
                                <div class="col-md-7 col-sm-12 el-input">
                                    <the-mask mask="+# (###) ###-##-##"
                                              class="form-control"
                                              placeholder="+7 (123) 456-78-99" name="phone" type="tel">

                                    </the-mask>
                                    @foreach ($errors->get('phone') as $message)
                                        <div><span class="help-block">{{ $message }}</span></div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('email'))has-error @endif">
                                <label for="email" class="col-md-3 cols-sm-12 control-label">Email</label>
                                <div class="col-md-7 col-sm-12 el-input">
                                    <input type="text" name="email" id="email"
                                           value = "{{Input::old('email')}}"
                                           class="form-control" placeholder="email@example.com">
                                    @foreach ($errors->get('email') as $message)
                                        <span class="help-block">{{ $message }}</span>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="box-footer" style="background: none">
                            <div class="form-group col-lg-2 col-lg-offset-8 col-sm-10 col-sm-offset-1 col-xs-12">
                                <button type="submit"
                                        class=" form-group btn input-block-level form-control btn-primary">
                                    Создать
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

@endsection
