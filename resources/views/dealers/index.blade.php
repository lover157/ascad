@extends('layouts.app')
@section('pageTitle', 'Дилеры')

@section('content')
    <div class="row">
        <div class="col-xl-12 col-md-12 col-xs-12">
            @include('dealers.menu')

            <div class="box box-info">
                <div class="box-header">
                    <h1 class="box-title">Дилеры</h1>
                </div>

                <div class="box-body">
                    <div class="row">
                        @foreach($dealers as $dealer)
                        <div class="col-md-4">
                            <div class="box">
                                <div class="box-header">
                                    <div class="box-title">
                                            <el-tooltip class="item" effect="dark" content="{{$dealer->name}} {{$dealer->second_name}}" placement="top-start">
                                                <img src="{{ Avatar::create($dealer->name.' '. $dealer->second_name)->toBase64() }}" width="30px"/>
                                            </el-tooltip>
                                        {{$dealer->name}} {{ $dealer->second_name}}
                                        <button type="button" class="btn btn-sm dropdown-toggle btn-actions"
                                                data-toggle="dropdown" aria-expanded="false">
                                            <i class="fa fa-cogs"></i>
                                            <span class="fa fa-caret-down"></span></button>
                                        <ul class="dropdown-menu dropdown-actions">
                                            <li><a href="{{route('dealers.edit', ['id'=>$dealer->id])}}"><i class="fa fa-pencil"></i>Изменить</a></li>
                                            <li><a href="#"  data-toggle="modal" data-target="#deleteDealer"
                                                   onclick="deleteDealer('{{$dealer->id}}', '{{$dealer->name}} {{$dealer->second_name}}')">
                                                    <i class="fa fa-trash"></i>Удалить</a>
                                            </li>
                                            <li class="divider"></li>
                                            <li><a href="#">Клиенты</a></li>
                                            <li><a href="#">Заказы</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <a href="mailto: {{ $dealer->email}}">
                                        email : {{ $dealer->email}}
                                    </a>
                                    <br />
                                    <a href="tel:+{{ $dealer->phone}}">
                                        phone : +{{ $dealer->phone}}
                                    </a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-warning" id="deleteDealer" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Удаление дилера</h4>
                </div>
                <form role="form" action="{{route('dealers.destroy')}}" method="POST"
                      enctype="multipart/form-data"
                      id="formUserDelete">
                    <div class="modal-body">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="delete">
                        <input type="hidden" value="" id="dealerId" name="id">

                        <p>Вы действительно хотите удалить дилера <br/>
                            <span class="text-bold" id="dealerName"></span> ?</p>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Отмена</button>
                        <button type="submit" class="btn btn-outline">Удалить дилера</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection


@section('scripts')
    <script>
        function deleteDealer(id, name) {
            $("#dealerName").text(name);
            $("#dealerId").val(id);
        }
    </script>
@endsection