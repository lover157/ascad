@permission('create-dealer')
<a href="{{route('dealers.create')}}" class="btn btn-success" style="margin-bottom: 10px">
    <i class="fa fa-plus" aria-hidden="true" title="Создать группу"></i> Создать дилера</a>
@endpermission