@extends('layouts.app')
@section('pageTitle', 'Создание новой роли')


@section('content')

    <div class="row">
        <div class="col-xs-12">
            @include('roles.menu')
            <form role="form" action="{{route('roles.store')}}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="box-body">
                    <div class="form-group">
                        <label for="name">Наименование (на англ без пробелов)</label>
                        <input type="text" class="form-control" placeholder="Название роли" name="name" id="name">
                    </div>
                    <div class="form-group">
                        <label for="display_name">Название</label>
                        <input type="text" class="form-control" placeholder="Название" name="display_name"
                               id="display_name">
                    </div>
                    <div class="form-group">
                        <label for="description">Описание</label>
                        <textarea name="description" class="form-control" placeholder="Описание роли" rows="3"
                                  id="description"></textarea>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer text-right" style="background: none">
                    <button type="submit" class="btn btn-primary">Создать</button>
                </div>
            </form>
        </div>
    </div>
@endsection