@extends('layouts.app')
@section('pageTitle', 'Группы пользователей')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            @include('roles.menu')
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Группы пользователей</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th class="hidden-xs">Наименование</th>
                            <th>Название</th>
                            <th class="hidden-xs">Описание</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($roles as $role)
                            <tr>
                                <td>{{$role['id']}}</td>
                                <td class="hidden-xs">{{$role['name']}}</td>
                                <td>{{$role['display_name']}}</td>
                                <td class="hidden-xs">{{$role['description']}}</td>
                                <td>
                                    <a href="{{route('roles.show', ['id' => $role['id']])}}"><i
                                                class="fa fa-eye fa-2x" aria-hidden="true"
                                                title="Просмотреть"></i></a>
                                    <a href="{{route('roles.edit', ['id' => $role['id']])}}"><i
                                                class="fa fa-pencil-square fa-2x" aria-hidden="true"
                                                title="Редактировать"></i></a>
                                    <a href="{{route('roles.destroy', ['id' => $role['id']])}}"><i
                                                class="fa fa-trash fa-2x" aria-hidden="true"
                                                title="Удалить"></i></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>id</th>
                            <th class="hidden-xs">Наименование</th>
                            <th>Название</th>
                            <th class="hidden-xs">Описание</th>
                            <th>Действия</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

@endsection