@extends('layouts.app')
@section('pageTitle', 'Роль Администратор')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Описание роли </h3>
                </div>
                <div class="box-body">
                    <p>{{$role->name}}</p>
                    <p>{{$role->display_name}}</p>
                    <p>{{$role->description}}</p>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Права</h3>
                </div>
                <div class="box-body">

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Название</th>
                            <th>Описание</th>
                            <th>Статус</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($allPermissions as $permission)
                            <tr>
                                <td>{{$permission->display_name}}</td>
                                <td>{{$permission->description}}</td>
                                <td class="text-center">
                                    @if(in_array($permission->id, $assignedPermissions))
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <ul>

                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Пользователи</h3>
                </div>
                <div class="box-body no-padding" style="display: block;">
                    <ul class="users-list clearfix">
                        @foreach($users as $user)
                        <li>
                            <a href="{{route('users.show', ['id'=> $user->id])}}">
                                <img src="/storage/avatars/{{$user->avatar}}" alt="{{$user->name}} {{$user->second_name}}">
                                <span class="users-list-name">{{$user->name}} {{$user->second_name}}</span>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                    <!-- /.users-list -->
                </div>
            </div>
        </div>
    </div>

@endsection