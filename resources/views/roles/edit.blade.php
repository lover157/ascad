@extends('layouts.app')
@section('pageTitle', 'Изменение роли '.$role->display_name)

@section('content')
    <form action="{{route('roles.update', ['id'=>$role->id])}}" method="post">
        {{csrf_field()}}
        {{method_field('put')}}
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Описание роли </h3>
                    </div>
                    <div class="box-body">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Наименование (на англ без пробелов)</label>
                                <input type="text" class="form-control" placeholder="Название роли" name="name" id="name" value="{{$role->name}}">
                            </div>
                            <div class="form-group">
                                <label for="display_name">Название</label>
                                <input type="text" class="form-control" placeholder="Название" name="display_name" value="{{$role->display_name}}" id="display_name">
                            </div>
                            <div class="form-group">
                                <label for="description">Описание</label>
                                <textarea name="description" class="form-control" placeholder="Описание роли" rows="3" id="description">{{$role->description}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Права</h3>
                    </div>
                    <div class="box-body">

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Название</th>
                                <th>Описание</th>
                                <th>Статус</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($allPermissions as $permission)
                                <tr>
                                    <td>{{$permission->display_name}}</td>
                                    <td>{{$permission->description}}</td>
                                    <td class="text-center">
                                        @if(in_array($permission->id, $assignedPermissions))
                                            <input type="checkbox" class="form-check-input" checked
                                                   name="permissions[]" value="{{$permission->id}}">

                                        @else
                                            <input type="checkbox" class="form-check-input" name="permissions[]" value="{{$permission->id}}">
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Пользователи</h3>
                    </div>
                    <div class="box-body no-padding" style="display: block;">
                        <ul class="users-list clearfix">
                            @foreach($users as $user)
                                <input type="checkbox" name="users[]" class="hidden" value="{{$user->id}}" checked>
                                <li>
                                    <a href="{{route('users.show', ['id'=> $user->id])}}">
                                        <img src="/storage/avatars/{{$user->avatar}}"
                                             alt="{{$user->name}} {{$user->second_name}}">
                                        <span class="users-list-name">{{$user->name}} {{$user->second_name}}</span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                        <!-- /.users-list -->
                    </div>
                </div>
            </div>
            <div class="col-md-5 text-right">
            <input type="submit" value="Изменить" class="btn btn-success"/>
            </div>
        </div>
    </form>
@endsection