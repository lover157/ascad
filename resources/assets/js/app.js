/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


window.Vue = require('vue');
Vue.use(require('vue-resource'));


import VueCordova from 'vue-cordova'
Vue.use(VueCordova);

import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/ru-Ru'
Vue.use(ElementUI, { locale });




import VueTheMask from 'vue-the-mask'
Vue.use(VueTheMask);

 /*
 *   My new components
 */

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('birthdate-component', require('./components/BirthDateComponent.vue'));
Vue.component('roles-component', require('./components/RolesChooseComponent.vue'));


Vue.component('order-component', require('./components/OrderComponent.vue'));
Vue.component('client-select-component', require('./components/ClientSelectComponent.vue'));


const app = new Vue({
    el: '#app'
});
