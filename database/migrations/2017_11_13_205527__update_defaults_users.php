<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDefaultsUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('banned')->default(0)->change();
            $table->boolean('activated')->default(0)->change();
            $table->string('password', 255)->nullable()->change();
            $table->string('avatar', 255)->default('no-image.png')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('banned')->change();
            $table->boolean('activated')->change();
            $table->string('password', 255)->change();
            $table->string('avatar', 255)->change();
        });
    }
}
