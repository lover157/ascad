<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $permissions = [
            ['name' => 'create-user', 'display_name' => 'Создание пользователей', 'description' => 'Право на создание пользователей', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'update-user', 'display_name' => 'Изменение пользователей', 'description' => '', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'delete-user', 'display_name' => 'Удаление пользователей', 'description' => '', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'ban-user', 'display_name' => 'Блокировка пользователей', 'description' => '', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],

            ['name' => 'crud-permissions', 'display_name' => 'Права', 'description' => 'Создание, удаление, изменение прав', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],

            ['name' => 'create-roles', 'display_name' => 'Создание ролей', 'description' => '', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'update-roles', 'display_name' => 'Изменение ролей', 'description' => '', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'delete-roles', 'display_name' => 'Удаление ролей', 'description' => '', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'show-roles', 'display_name' => 'Просмотр ролей', 'description' => '', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'assign-roles', 'display_name' => 'Назначение ролей', 'description' => '', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],


            ['name' => 'create-dealer', 'display_name' => 'Создание диллеров', 'description' => '', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'create-clients', 'display_name' => 'Создание клиентов', 'description' => '', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'create-projects', 'display_name' => 'Создание заказов', 'description' => '', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            
        ];

        foreach ($permissions as $permission) {
            DB::table('permissions')->insert([
                $permission
            ]);
        }


    }
}
