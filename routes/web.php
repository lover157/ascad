<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/welcome', function () {
    return view('welcome');
});
Auth::routes();

Route::get('/test', function () {

    return view('test.worklist');
});


Route::get('auth/{provider}', 'Auth\SocializeController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\SocializeController@handleProviderCallback');

Route::middleware(['auth', 'activated', 'banned'])->group(function () {

    Route::get('/', 'HomeController@index')->name('home');

    //DEALERS
    Route::prefix('dealers')->group(function () {
        Route::get('', 'DealersController@index')->name('dealers.index');
        Route::get('/create', 'DealersController@create')->name('dealers.create');
        Route::get('/show/{id}', 'DealersController@show')->name('dealers.show');
        Route::get('/edit/{id}', 'DealersController@edit')->name('dealers.edit');
        Route::post('/store/', 'DealersController@store')->name('dealers.store');
        Route::put('/update/{id}', 'DealersController@update')->name('dealers.update');
        Route::delete('/destroy/', 'DealersController@destroy')->name('dealers.destroy');
    });


    Route::prefix('api')->group(function () {

        Route::prefix('worklist')->group(function () {
            Route::get('/', 'ServicesController@getList');
            Route::get('/{id}', 'ServicesController@apiGetById');


        });

        Route::prefix('client')->group(function(){
            Route::get('/phone/{phone}', 'ClientsController@findByPhone');
        });

    });
    //CLIENTS
    Route::prefix('clients')->group(function () {
        Route::get('', 'ClientsController@index')->name('clients.index');
        Route::get('/create', 'ClientsController@create')->name('clients.create');
        Route::get('/show/{id}', 'ClientsController@show')->name('clients.show');
        Route::get('/edit/{id}', 'ClientsController@edit')->name('clients.edit');
        Route::post('/store/', 'ClientsController@store')->name('clients.store');
        Route::put('/update/{id}', 'ClientsController@update')->name('clients.update');
        Route::delete('/destroy/', 'ClientsController@destroy')->name('clients.destroy');
        Route::get('/list', 'ClientsController@list')->name('clients.list');
    });

    //Addresses
    Route::prefix('addresses')->group(function () {
        Route::post('/store', 'AddressesController@store')->name('addresses.store');
        Route::put('/update', 'AddressesController@update')->name('addresses.update');
        Route::delete('/destroy', 'AddressesController@destroy')->name('addresses.destroy');
        Route::get('/client/{id}', 'AddressesController@byClient')->name('addressesByClient');
    });
    //ORDERS
    Route::prefix('orders')->group(function () {
        Route::get('', 'OrdersController@index')->name('orders.index');
        Route::get('/create', 'OrdersController@create')->name('orders.create');
        Route::get('/show/{id}', 'OrdersController@show')->name('orders.show');
        Route::get('/edit/{id}', 'OrdersController@edit')->name('orders.edit');
        Route::post('/store', 'OrdersController@store')->name('orders.store');
        Route::put('/update/{id}', 'OrdersController@update')->name('orders.update');
        Route::delete('/destroy/{id}', 'OrdersController@destroy')->name('orders.destroy');

    });
    //ROLES
    Route::prefix('roles')->group(function () {

        Route::get('', 'RolesController@index')->name('roles.index');
        Route::get('/create', 'RolesController@create')->name('roles.create');
        Route::get('/show/{id}', 'RolesController@show')->name('roles.show');
        Route::get('/edit/{id}', 'RolesController@edit')->name('roles.edit');
        Route::post('/store/', 'RolesController@store')->name('roles.store');
        Route::put('/update/{id}', 'RolesController@update')->name('roles.update');
        Route::delete('/destroy/{id}', 'RolesController@destroy')->name('roles.destroy');
    });

    //USERS
    Route::prefix('users')->group(function () {
        Route::get('', 'UsersController@index')->name('users.index');
        Route::get('/create', 'UsersController@create')->name('users.create')->middleware(['permission:create-user']);
        Route::get('/show/{id}', 'UsersController@show')->name('users.show');
        Route::get('/edit/{id}', 'UsersController@edit')->name('users.edit');
        Route::post('/store/', 'UsersController@store')->name('users.store')->middleware('permission:create-user');
        Route::put('/update/{id}', 'UsersController@update')->name('users.update');
        Route::delete('/destroy', 'UsersController@destroy')->name('users.destroy')->middleware('permission:delete-user');
        Route::post('/ban/{id}', 'UsersController@ban')->name('users.ban')->middleware('permission:ban-user');
        Route::post('/unban/{id}', 'UsersController@unban')->name('users.unBan')->middleware('permission:ban-user');

    });

    //Permissions
    Route::prefix('permissions')->group(function () {
//        Route::get('', 'UsersController@index')->name('permissions.index')->middleware(['permission:acl']);
        Route::get('', 'PermissionsController@index')->name('permissions.index')->middleware(['permission:assign-roles']);
        Route::get('/create', 'PermissionsController@create')->name('permissions.create');
        Route::get('/show/{id}', 'PermissionsController@show')->name('permissions.show');
        Route::get('/edit/{id}', 'PermissionsController@edit')->name('permissions.edit');
        Route::post('/store/', 'PermissionsController@store')->name('permissions.store');
        Route::put('/update/{id}', 'PermissionsController@update')->name('permissions.update');
        Route::get('/destroy/{id}', 'PermissionsController@destroy')->name('permissions.destroy')->middleware(['permission:delete-permission']);
    });


    //Settings
    Route::prefix('settings')->group(function () {

        //Services
        Route::prefix('services')->group(function () {
            Route::get('', 'ServicesController@index')->name('services.index');
            Route::get('/create', 'ServicesController@create')->name('services.create');
            Route::post('/store', 'ServicesController@store')->name('services.store');
            Route::get('/edit/{id}', 'ServicesController@edit')->name('services.edit');
            Route::put('/update/{id}', 'ServicesController@update')->name('services.update');
            Route::delete('/destroy', 'ServicesController@destroy')->name('services.destroy');
        });
    });


    //SideBar toggle
    Route::get('/menu-toggle', function () {
        $value = Cookie::get('sidebar-menu');
        if ($value === 'sidebar-collapse') {
            Cookie::queue(Cookie::make('sidebar-menu', '', '50000'));
        } else {
            Cookie::queue(Cookie::make('sidebar-menu', 'sidebar-collapse', '50000'));
        }
        return;
    })->name('sidebar-menu');
});


Route::get('/profile/{hash}', 'ProfileController@create')->name('profile.create');
Route::put('/profile/update', 'ProfileController@update')->name('profile.update');


Route::get('/banned', function () {
    return view('errors.banned');
})->name('user-banned');

Route::get('/not-activated', function () {
    return view('errors.not-activated');
})->name('not-activated');






